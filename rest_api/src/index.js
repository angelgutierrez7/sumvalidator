const express = require('express')
const bodyparser = require('body-parser')

const app = express()

app.get('/', function(request,response){
    response.send('HOLA MUNDO DESDE API REST')    
})

app.get('/:num1/:num2', (request,response)=>{
    const value1 = request.params.num1
    const value2 = request.params.num2
    const sum = Number(value1) + Number(value2)
    response.json({result:sum})    
})
app.listen(3001, function(){
    console.log('server is running in port 3001')
})